# Box Auth
A tool to help you authenticate to Box API v2 over oauth.
This can be used as a library for your own code or executable to get access tokens and refresh tokens.
This is useful if you want to use box api headlessly in scripts without people having to log in.
Supports python 2 and 3.

# Installation
`pip install 'box-auth>=0.1.0'`

# Use it in code:
```
from box_auth.box_auth import BoxAuth

client_id = "your client id"
client_secret = "your client secret"
username = "your box email"
password = "your password"
box_auth = BoxAuth(client_id, client_secret, username, password)

access_token = box_auth.get_access_token()
```

You can call get_access_token as often as needed, if it expired it will get a new one.


# Use on command line
```
python box_auth.py <<< '
{
  "client-id": "your client id",
  "client-secret": "your client secret",
  "username": "your box email",
  "password": "your box password"
}'

outputs:
{'access_token': 'the access token', 'refresh_token': 'the_refresh_token'}
```

The JSON keys can also be supplied as command line arguments. You can mix and match command line arguments and STDIN.

You should avoid sending the password as a command line argument outside of testing.

```
python box_auth.py --client-id "your client id" --username "username" <<< '
{
  "client-secret": "your client secret",
  "password": "your box password"
}'
```

Pro Tip: Store your credentials in a file
```
echo '{
        "client-id": "your client id",
        "client-secret": "your client secret",
        "username": "your box email",
        "password": "your box password"
      }' > box_credentials.json
python box_auth.py < box_credentials.json
```
