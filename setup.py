#!/usr/bin/env python

from distutils.core import setup

setup(
    name="box-auth",
    packages=["box_auth"],
    version="0.1.0",
    description="Headless box api v2 auth provider",
    author="Jordan Sterling",
    author_email="jordan@jordansterling.com",
    url="https://bitbucket.org/fool/box-auth",
    keywords=["box", "api", "oauth2", "sdk"],
    license="MIT",
)

